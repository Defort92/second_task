import uuid
from django.db import models
from django.contrib.auth.models import UserManager as DjangoUserManager

class AbstractBaseModel(models.Model):
    id = models.BigAutoField(primary_key=True, unique=True)

    def __str__(self):
        return str(self.id)

    class Meta:
        abstract = True


class UserManager(DjangoUserManager):
    pass


class User(AbstractBaseModel):
    objects = UserManager()
    token = models.UUIDField(default=uuid.uuid4, editable=False)
    pass


class Pool(AbstractBaseModel):
    pass


class Vote(AbstractBaseModel):
    pass
