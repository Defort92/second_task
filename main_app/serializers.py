from rest_framework import serializers
from main_app.models import User, Poll, Vote


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User


class PollSerializer(serializers.ModelSerializer):
    class Meta:
        model = Poll


class VoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vote
